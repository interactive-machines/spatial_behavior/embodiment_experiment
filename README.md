# Unity Project Info

## Example Images from Study
See this [Google Drive link](https://drive.google.com/drive/u/1/folders/1cHfzLymNVUKo2N2UH8OPultXW8qSIOCi) to view the images we generated from this code to use in our study.

## Requirements
- Valid ROS distribution (Tested on ROS Melodic with Ubuntu 18.04)
- [Rosbridge Server](http://wiki.ros.org/rosbridge_suite) Linux Package 
- Unity Editor (Tested on Unity version 2019.4.0f1)

## Usage

- Install the correct unity version (it will prompt in UnityHub)

- Open the `unity_project` folder as a Unity project.

- Initialize scene assets in terminal with:

```
git submodule update --init
```
Note: many of these assets are from the [Microsoft Rocketbox Avatar Library](https://github.com/microsoft/Microsoft-Rocketbox)

- Load the scene from the scenes folder by using `Open Scene` in the Unity editor and selecting `Assets > Scenes > CocktailParty.unity`

- Start a rosbridge websocket

```
roslaunch rosbridge_server rosbridge_websocket.launch
```

- Check the "Use Robot" checkbox in the `PersonManager` -> `Tracked Persons Subcriber` object if you want to replace a specific person with a robot (and change the `Use Robot As Person` index (0-based) if you want to replace a different person). Note that you will need to use the `Robot Height Offset` parameter to adjust some robot's height in the scene, as gravity does not apply to all models. You will also need to change the `Robot Prefab` based on which robot you want. See the image below for how to access these parameters.

![Use Robot](unity_project/docs/img/robotparameters.png?raw=true)

- Hit play

- Run `rosrun publish_data_unity_package publish_positions.py` or `rosrun publish_data_to_unity publish_csv_positions.py` (from `publish_data_to_unity` ROS package, see below for details)


# Publish Data to Unity ROS Package Info

## Rendering Cocktail Party groups

To spawn people in the scene, run

    rosrun publish_data_to_unity publish_positions.py

![cocktailparty](publish_data_to_unity/docs/cocktailparty.gif)


## Rendering data from CSV file

To render groups with features from a csv file, run: 

    rosrun publish_data_to_unity publish_csv_positions.py _csv:=render_exp.csv _root_output:=recordings/male/real _camera:=low

By default, the parameter `csv` above is set to render_pilot.csv, which corresponds to the pilot data. The parameter `root_output` is set to recordings/male/real by default (this is where resulting image data will be saved to from the package root). The `camera` parameter, which determines the camera the images are rendered from in the scene, is set to `top` by default (the other options are `side` and `low`). Note, you need to switch the display in Unity (menu on the top left of the game view window) to match the camera angle you choose for Unity to actually capture the images from that angle.
