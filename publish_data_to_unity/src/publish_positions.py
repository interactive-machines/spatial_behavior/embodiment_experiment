#!/usr/bin/env python
'''
publish cocktail party positions to unity

download the following into the `data` folder:
  - `grouping.ref` -- https://drive.google.com/file/d/0Bzf1l8WmTwu0ZUdMOFRiZ1JyS3M/view
  - `tracking_hp.log` --  https://drive.google.com/file/d/0Bzf1l8WmTwu0ZWxZdVZ0RUFFNm8/view
'''
import actionlib
import errno
import numpy as np
import os
import re
import rospy
import tf
import tf.transformations as tr

ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))

from cocktailparty_to_unity.msg import TrackedPerson, TrackedPersons

class PublishPositions(object):
    def __init__(self):
        rospy.init_node("publish_positions")

        self.data_folder = os.path.join(ROOT_DIR, rospy.get_param('~data_folder', 'data'))
        print("data folder: {}".format(self.data_folder))

        self.people_pub = rospy.Publisher("/publish_positions/tracked_persons", TrackedPersons, queue_size=10)

        self.read_data()

        #rospy.spin()


    def read_data(self, groups_filename='grouping.ref', features_filename='features.txt'):
        """
        Helper function to load group data from text files
        :param features_filename: path to file with individual features
        :param groups_filename: path to groups
        :return: tuple with features and group data organized as dictionaries, list of people's ids, and num features
        :note: this function filters the feature frames to only return those matching group timestamps

        from: binary_cocktail_party.ipynb
        """
        groups_path = os.path.join(self.data_folder, groups_filename)
        groups_per_frame = np.genfromtxt(groups_path, dtype='str', delimiter=',')
        features_path = os.path.join(self.data_folder, features_filename)
        features_per_frame = np.genfromtxt(features_path, dtype='str')

        # convert group data to dict
        self.stamps = []
        self.groups_dict = {}
        for groups in groups_per_frame:
            groups_arr = re.split(" < | > < ", groups)
            s = groups_arr[0]

            self.stamps.append(s)
            self.groups_dict[s] = []

            last_index = -1
            for group in groups_arr[1:]:
                last_index += 1
                self.groups_dict[s].append(re.split(" ", group))

            # remove last > character
            if len(groups_arr[1:]) == 0:
                continue

            self.groups_dict[s][last_index] = self.groups_dict[s][last_index][:-1]

        # filter features frames
        self.features_dict = {}
        self.features_stamps = list(features_per_frame[:, 0])
        self.num_features = None
        pid_set = set()

        def check_num_features(num_features, f):
            """Helper function to update the number of features estimated so far and check for consistency"""
            if num_features is None:
                num_features = len(f)
            elif num_features != len(f):
                raise RuntimeError("Inconsistent number of features (got {} but expected {})".
                                   format(len(f), num_features))
            return num_features

        for s in self.stamps:
            # get features data for the given stamp
            frame_idx = self.features_stamps.index(s)
            people = features_per_frame[frame_idx][1:]
            features = {}

            # parse people list
            pid = None
            f = []
            for i in range(len(people)):
                if people[i][0:2] == "ID":
                    if pid is not None:
                        features[pid] = f
                        self.num_features = check_num_features(self.num_features, f)
                    pid = people[i]
                    f = []
                else:
                    f.append(float(people[i]))
            if pid is not None:
                features[pid] = f
                self.num_features = check_num_features(self.num_features, f)

            # save people's identifiers
            for identifier in features.keys():
                if identifier not in pid_set:
                    pid_set.add(identifier)

            # store features for this frame
            self.features_dict[s] = features

        # convert person ids from set to list
        self.person_ids = list(pid_set)
        self.person_ids.sort()

        #return features_dict, groups_dict, person_ids, num_features

    def fnow(self):
        now = rospy.get_rostime()
        return float('.'.join([str(now.secs), str(now.nsecs)]))


    def publish_all(self):
        # playback at the same rate as recorded
        offset = self.fnow() - float(self.stamps[0])
        for stamp in self.stamps:
            if self.fnow() <= float(stamp) + offset:
                rospy.sleep(.1)
            #print("[%s] publishing %s"%(stamp, self.features_dict[stamp]))
            people = TrackedPersons()
            people.header.stamp = rospy.get_rostime()

            # iterate over one entry
            #   e.g.  print(self.features_dict[stamp])
            #         {'ID_001': [0.84, 4.18, 1.37], 'ID_002': [1.55, 3.68, 1.71], 'ID_003': [1.97, 4.33, 2.63], 'ID_004': [0.99, 5.12, -0.78], 'ID_005': [1.5, 5.3, -2.74], 'ID_006': [2.21, 5.09, 2.56]}
            #   where the format is 'track_id': [x, y, orientation]
            for track_id, f in self.features_dict[stamp].items():
                x = f[0]
                y = f[1]
                th = f[3] # use body orientation instead of head (head is f[2])
                track = TrackedPerson()
                track.track_id = int(track_id.split('_')[-1])
                track.pose.position.x = x
                track.pose.position.y = y
                track.pose.position.z = 0
                R = tr.rotation_matrix(th, (0,0,1))
                q = tr.quaternion_from_matrix(R)
                track.pose.orientation.x = q[0]
                track.pose.orientation.y = q[1]
                track.pose.orientation.z = q[2]
                track.pose.orientation.w = q[3]
                people.tracks.append(track)
            self.people_pub.publish(people)
            if rospy.is_shutdown():
                break

            rospy.sleep(1)

if __name__ == "__main__":
    try:
        node = PublishPositions()
        node.publish_all()
    except rospy.ROSInterruptException:
        pass

