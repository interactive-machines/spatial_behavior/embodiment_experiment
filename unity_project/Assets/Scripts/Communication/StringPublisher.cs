using UnityEngine;

namespace RosSharp.RosBridgeClient
{
    public class StringPublisher : UnityPublisher<MessageTypes.Std.String>
    {
        public string publishedString;
        private MessageTypes.Std.String message;

        protected override void Start()
        {
            base.Start();
            InitializeMessage();
        }

        private void FixedUpdate()
        {
            UpdateMessage();
        }

        private void InitializeMessage()
        {
            message = new MessageTypes.Std.String();
            message.data = "";
        }

        private void UpdateMessage()
        {
            message.data = publishedString;
            Publish(message);
        }
    }
}
