/* 
 * This message is auto generated by ROS#. Please DO NOT modify.
 * Note:
 * - Comments from the original code will be written in their own line 
 * - Variable sized arrays will be initialized to array of size 0 
 * Please report any issues at 
 * <https://github.com/siemens/ros-sharp> 
 */

using Newtonsoft.Json;

using RosSharp.RosBridgeClient.MessageTypes.Std;

namespace RosSharp.RosBridgeClient.MessageTypes.CocktailpartyToUnity
{
    public class TrackedPersons : Message
    {
        [JsonIgnore]
        public const string RosMessageName = "cocktailparty_to_unity/TrackedPersons";

        //  from: https://github.com/spencer-project/spencer_people_tracking/blob/master/messages/spencer_tracking_msgs/msg/TrackedPersons.msg
        //  Message with all currently tracked persons 
        // 
        public Header header;
        //  Header containing timestamp etc. of this message
        public TrackedPerson[] tracks;
        //  All persons that are currently being tracked

        public TrackedPersons()
        {
            this.header = new Header();
            this.tracks = new TrackedPerson[0];
        }

        public TrackedPersons(Header header, TrackedPerson[] tracks)
        {
            this.header = header;
            this.tracks = tracks;
        }
    }
}
